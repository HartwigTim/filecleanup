"""library of functions used in main file.

Raises:
    Exception: [description]
    Exception: [description]
    Exception: [description]
    Exception: [description]
    Exception: [description]
    Exception: [description]
"""

import os
import errno
import stat
import shutil
import glob
import time

from pathlib import Path, PurePath
import pandas as pd
import numpy as np

import progressbar

from checksumdir import dirhash


def handleRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
        os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  # 0777
        func(path)
    else:
        raise Exception("Could not remove ReadOnly from directory.")


def path_checker(path, *args):
    """Simple function that checks whether a file or dir exists.

    Args:
        path (str): path to dir or file
        2nd_path (str): path to dir or file
        format (str): the file is expected to be

    Raises:
        Exception: Message to the use
    """

    if Path(path).exists() is False:
        raise Exception(
            "The path does not exists: " + path)

    if len(args) > 0:
        if Path(path).suffix not in args:
            raise Exception(
                "The file format is not accepted. Only: " + ''.join(args))


def paths_relation(path, otherpath):
    """ Simple function that checks the relation(either parent or child)
    of two path.

    Args:
        path (str): Valid file or directory 
        otherpath (str): 2nd valid file or directory

    Raises:
        Exception: If the two path are relative, raise. 

    Returns:
        [True]: If the paths are not relativ. 
    """

    if PurePath(otherpath).is_relative_to(path) or PurePath(path).is_relative_to(otherpath):
        raise Exception(
            'The two path cannot be related to each other. \n 1st Path: ' + path + '\n 2nd Path: ' + otherpath)
    return True


def delete_by_path(path, deletion_mark):
    if str(path) == 'nan' or str(deletion_mark) == ' ':
        return

    if Path(path).is_dir():
        try:
            shutil.rmtree(path, ignore_errors=False,
                          onerror=handleRemoveReadonly)
            return 'deleted successfully'

        except FileNotFoundError:
            return 'file could not be found'

    if deletion_mark == 'x' or deletion_mark == 'y':
        try:
            Path(path).unlink()
            return 'deleted successfully'

        except FileNotFoundError:
            return 'file could not be found'

    #     print(
    #         "A entire directory has been identified." + str(path) + " \n " + "Do you really want to delete this? [y/n]")
    #     user_input = ""
    #     while user_input != "n" and user_input != "y":
    #         user_input = input()
    #         if user_input == "n":
    #             exit()

    #         elif user_input != "y":
    #             print(
    #                 "The input must be either 'y' or 'n'. Do you want to proceed? [y/n]")


def delete_by_list(path):
    """Deletes all files listed in an xls or csv list of duplicates and
    marked with \" y \". if it is a direcory, user confirmation will be requested before deletion.

    Args:
        path ([str]): [description]

    Raises:
        Exception: [description]
        Exception: [description]
        Exception: [description]
        Exception: [description]
    """

    # TODO If all dirs with the same hash are marked to be deleted, ask if the use really wants this.
    # TODO remove format checking from path checker

    path_checker(path, '.xlsx', '.csv')

    delete_file = Path(path)
    if delete_file.suffix == ".xlsx":
        duplicates = pd.read_excel(delete_file)

    elif delete_file.suffix == ".csv":
        duplicates = pd.read_csv(delete_file)

    else:
        raise Exception(
            "The file could not be read or a wrong file type was given.")

    if duplicates.empty is True:
        raise Exception("The data given is empty.")

    duplicates['deletion_status'] = duplicates.apply(
        lambda x: delete_by_path(x['path'], x['to_be_deleted']), axis=1)

    # # used for the compareall function call

    # for i in range(int(duplicates['count'].max())):
    #     duplicates['deletion_status_'+str(i)] = duplicates.apply(
    #         lambda x: delete_by_path(
    #             x['path_' + str(i)], x['to_be_deleted_' + str(i)]), axis=1)

    # Deletion Report

    writer = pd.ExcelWriter('duplicates-all.xlsx', engine='xlsxwriter')
    duplicates.to_excel(writer, sheet_name='Sheet1',
                        startrow=1, header=False, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    # workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Get the dimensions of the dataframe.
    (max_row, max_col) = duplicates.shape

    # Create a list of column headers, to use in add_table().
    # column_settings = [{'header': Index}]
    column_settings = [{'header': column} for column in duplicates.columns]

    # Add the Excel table structure. Pandas will add the data.
    worksheet.add_table(0, 0, max_row, max_col - 1,
                        {'columns': column_settings})

    worksheet.set_column(0, 0, 125)
    worksheet.set_column(1, 1, 35)
    worksheet.set_column(2, 2, 10)
    worksheet.set_column(3, 3, 10)
    worksheet.set_column(4, 4, 10)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()


def compare(path_origin, path_to_compare, target="duplicates.xlsx", formats=['*']):
    """Compares two directories with each other and looks for duplicated files.
    Such will be printed in an xls or csv format.

    Args:
        path_origin (str): The path containing all files that shall be used for
        the search.
        path_to_compare (str): The path where duplicates shall be looked for
        target (str): Path to the location to store the duplication information file.
        formats (:type: list of str, optional): File formats to include in search.

    Returns:
        :Bool: True if the xls/csv file was successfully created.
        :Bool: False if the xls/csv file was not created

    """
    # TODO Write Unit Test
    # TODO THink about return dataframe object for further use.
    # TODO Catch Permission Error
    # TODO improve the checking.

    # Input Validation
    path_checker(path_origin)
    path_checker(path_to_compare)
    paths_relation(path_origin, path_to_compare)

    original_files = []
    for file_format in formats:
        original_files += list(Path(path_origin).glob('**/*.' + file_format))

    comparison_files = []
    for file_format in formats:
        comparison_files += list(
            Path(path_to_compare).glob('**/*.' + file_format))

    count_to_search = len(original_files)
    print("The number of files to look for is: " + str(count_to_search))

    count_to_compare = len(comparison_files)
    print("The number of files to compare to is: " + str(count_to_compare))

    print("The calculation time is estimated to be: " +
          str(2 * count_to_compare * count_to_search / 1000) + " seconds")

    # Search
    data = {'original': [],
            'duplicate': [],
            'size': [],
            'to_be_deleted': []
            }

    for file in original_files:
        duplicates = Path(path_to_compare).glob("**/" + file.name)

        for duplicate in duplicates:
            if str(file) != str(duplicate) and file.stat().st_size == duplicate.stat().st_size:
                data["original"].append(str(file))
                data["duplicate"].append(str(duplicate))
                data["size"].append(duplicate.stat().st_size)
                data["to_be_deleted"].append(' ')

    try:
        pd.DataFrame(data).to_excel(target)
    except PermissionError:
        raise Exception("Close the spreadsheet and run again.")

    if Path(target).exists():
        print("The comparison file was created in the folder: XYZ")
    else:
        print("xls could not be created.")

    os.system("pause")


def find_duplicates(duplicates, duplicates2,  i):

    x = 0
    for duplicate in duplicates:
        if i == x:
            return str(duplicate)
            # duplicates_data.append(str(duplicate))
            # index_heading.append("path_" + str(i))
        x += 1

    for duplicate in duplicates2:
        if i == x:
            return str(duplicate)
            # duplicates_data.append(str(duplicate))
            # index_heading.append("path_" + str(i))
        x += 1


# def compareall(path, path2=' ', target="duplicates-all.xlsx", formats=['*']):
#     """Searchs for all duplicate files in a give path. The results will be printed to an xlsx.
#     file in the target path. Files with no duplicate will not be listed.

#     Args:
#         path (str): Path to be screened
#         path2 (str): Path to be screened too
#         target (str, optional): Path to store the result. Defaults to "duplicates-all.xlsx".
#         formats (list, optional): Formats to be included in the search. Defaults to ['jpg', 'jpeg', 'pdf'].
#     """

#     path_checker(path, otherpath=path2)
#     # TODO path 2 cannot be a child of path -> double entry of same file
#     if path2 != ' ':
#         path_checker(path2)

#     if PurePath(path2).is_relative_to(path) or PurePath(path).is_relative_to(path2):
#         raise Exception('The two path cannot be related to each other. ')

#     comparison_files = []
#     for file_format in formats:
#         comparison_files += list(
#             Path(path).glob('**/*.' + file_format))

#         if path2 != ' ':
#             comparison_files += list(
#                 Path(path2).glob('**/*.' + file_format))

#     data = {'size': [],
#             'duplicate_stem': []
#             }

#     for file in comparison_files:
#         if file.is_file():
#             data["size"].append(file.stat().st_size)
#             data["duplicate_stem"].append(str(file.name))

#     pd.DataFrame(data).to_excel("entire_list.xlsx")

#     all_files = pd.DataFrame(data).groupby(
#         ['duplicate_stem', 'size']).size().reset_index(name='count')

#     all_files.drop(
#         all_files.index[all_files['count'] == 1], inplace=True)

#     if str(all_files['count'].max()) == 'nan':
#         raise Exception("no duplicate file was found. ")

#     progbar = progressbar.ProgressBar(maxval=(all_files['count'].max()),
#                                       widgets=[progressbar.Bar('=', '[', ']'),
#                                       ' ',
#                                                progressbar.Percentage()])
#     progbar.start()

#     # fill the duplicate with data
#     path_header = []
#     deletion_header = []
#     for i in range(all_files['count'].max()):

#         progbar.update(i+1)

#         all_files['path_' + str(i)] = all_files.apply(
#             lambda x: find_duplicates(
#                 Path(path).glob('**/' + x['duplicate_stem']),
#                 Path(path2).glob('**/' + x['duplicate_stem']),
#                 i),
#             axis=1)
#         all_files['to_be_deleted_' + str(i)] = ''

#         path_header.append('path_'+str(i))
#         deletion_header.append('to_be_deleted_'+str(i))

#     paths = all_files[path_header].copy().stack(
#         dropna=False).to_frame().reset_index(drop=True)
#     paths['path'] = ' '
#     paths['to_be_deleted'] = ' '

#     paths.replace('', np.nan, inplace=True)

#     paths.dropna(how='all', inplace=True)

#     # Change Print Out

#     writer = pd.ExcelWriter('duplicates-all.xlsx', engine='xlsxwriter')

#     paths.to_excel(writer, sheet_name='Sheet1',
#                    startrow=1, header=False, index=True)

#     # Get the xlsxwriter workbook and worksheet objects.
#     worksheet = writer.sheets['Sheet1']

#     # Get the dimensions of the dataframe.
#     (max_row, max_col) = paths.shape

#     # Create a list of column headers, to use in add_table().
#     column_settings = [{'header': column} for column in paths.columns]

#     # Add the Excel table structure. Pandas will add the data.
#     worksheet.add_table(0, 0, max_row, max_col - 1,
#                         {'columns': column_settings})

#     # Make the columns wider for clarity.
#     # worksheet.set_column(0, max_col - 1, 12)

#     # Auto-adjust columns' width
#     # for column in paths:
#     #     column_width = 100
#     #     # column_width = max(paths[column].astype(
#     #     #     str).map(len).max(), len(column))
#     #     col_idx = paths.columns.get_loc(column)
#     # # worksheet.set_column(col_idx, col_idx, column_width)
#     #     worksheet.set_column(col_idx, col_idx, column_width)

#     worksheet.set_column(0, 0, 5)
#     worksheet.set_column(1, 1, 100)
#     worksheet.set_column(2, 2, 10)

#     # Close the Pandas Excel writer and output the Excel file.
#     writer.save()

#     progbar.finish()

#     print('Result file successfully created')


def comparealldirs(path, target="duplicates-all.xlsx", paths=[]):
    """Searchs for all directories/subdirectories with the exact same
    content in a give path. The results will be printed to an xlsx
    file in the target path. Directories with no duplicate will not be
    listed.

    Args:
        path (str): Path to be screened
        target (str, optional): Path to store the result. Defaults to "duplicates-all.xlsx".
        paths (list str): List of paths to be screened too
    """

    routine_time = time.time()

    path_checker(path)

    for directory in paths:
        # TODO to be improvedsuch that all are checked againsts each other.
        paths_relation(path, directory)

    list_dirs = glob.glob(f'{path}/*/**/', recursive=True)
    # list_dirs = Path(path).glob('/*/**/')

    for directory in paths:
        list_dirs += glob.glob(f'{directory}/*/**/', recursive=True)

    data = {'path': []}

    progbar = progressbar.ProgressBar(maxval=(3),
                                      widgets=[progressbar.Bar('=', '[', ']'),
                                               ' ',
                                               progressbar.Percentage()])

    for directory in list_dirs:
        data["path"].append(str(Path(directory)))

    duplicates = pd.DataFrame(data)

    # Hashing longest calc time
    progbar.start()
    progbar.update(1)
    duplicates['hash'] = duplicates.apply(
        lambda x: dirhash(x['path'], 'md5'), axis=1)
    progbar.update(2)

    duplicates["to_be_deleted"] = ' '

    # Filter to delete non-duplicates
    duplicates["duplicate"] = duplicates['hash'].duplicated(keep=False)
    duplicates = duplicates[duplicates["duplicate"] == True]

    duplicates.sort_values(by=['hash'], inplace=True)
    duplicates.reset_index(drop=True, inplace=True)

    # Removes duplicates children with same hash/content e.g.
    # C:\Users\levinho\Desktop\Tim\Ausbildung
    # C:\Users\levinho\Desktop\Tim\Ausbildung\Abrechnung
    # Ausbildung only contains the dir Abrechnung

    list_of_children = []
    for count, row in enumerate(duplicates.itertuples()):
        if count == 0:
            lastrow = row
            continue

        if row.hash == lastrow.hash:
            # Parent Check
            root = Path(lastrow.path)
            child = Path(row.path)

            root_lastrow = Path(row.path)
            child_row = Path(lastrow.path)

            if root in child.parents or root_lastrow in child_row.parents:
                # remember index
                list_of_children.append(row.Index)
                # lastrow remains
            else:
                lastrow = row
        else:
            lastrow = row

    # Drops all children from the dataframe
    duplicates.drop(duplicates.index[list_of_children], inplace=True)

    # Redo filter to delete non-duplicates
    duplicates["duplicate_new"] = duplicates['hash'].duplicated(keep=False)
    duplicates = duplicates[duplicates["duplicate_new"] == True]

    duplicates.sort_values(by=['hash'], inplace=True)
    duplicates.drop(['duplicate', 'duplicate_new'], axis=1, inplace=True)

    # Change Print Out
    writer = pd.ExcelWriter(target, engine='xlsxwriter')
    duplicates.to_excel(writer, sheet_name='Sheet1',
                        startrow=1, header=False, index=False)

    # Get the xlsxwriter workbook and worksheet objects.
    # workbook = writer.book
    worksheet = writer.sheets['Sheet1']

    # Get the dimensions of the dataframe.
    (max_row, max_col) = duplicates.shape

    # Create a list of column headers, to use in add_table().
    # column_settings = [{'header': Index}]
    column_settings = [{'header': column} for column in duplicates.columns]

    # Add the Excel table structure. Pandas will add the data.
    worksheet.add_table(0, 0, max_row, max_col - 1,
                        {'columns': column_settings})

    worksheet.set_column(0, 0, 125)
    worksheet.set_column(1, 1, 35)
    worksheet.set_column(2, 2, 10)
    worksheet.set_column(3, 3, 10)

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()

    progbar.finish()

    print('Result file successfully created')
