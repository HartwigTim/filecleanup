"""Tool to search different paths for duplicate files. Such will be listed
 in xls files and by manually adjusting
 the to be deleted column the tool will delete the ticked once.
    """


import argparse
import time

from .helper import compare, comparealldirs, delete_by_list


def main():
    """Handling and directing of command-line arguments
    """
    parser = argparse.ArgumentParser(prog='filecleanup',
                                     description='Analyzing and deletion of duplicates in various paths given.'
                                     'Three step process: '
                                     '1st: Analysis via comparealldir, etc. \n '
                                     '2nd: (manual) marking of to be deleted \n '
                                     '3rd: Deletion of marked as per deletion overview. \n'
                                     )

    parser.add_argument('-compare', nargs=2, type=str,
                        metavar=('<dir_origin>', '<dir_compare>'),
                        help='Compares all files in the origional directory.'
                        'Creates overview table.')

    parser.add_argument('-compareall', nargs=2, type=str,
                        metavar=('<dir_comparison>', '<2nd dir_comparison>'),
                        help='Compares all files in name and size a given '
                        'directory. Output is an overview table')

    parser.add_argument('-comparealldirs', nargs=1, type=str,
                        metavar=('<files (default)>'),
                        help="Search for duplicate directories in one or multiple paths.")

    parser.add_argument('-delete', nargs=1, type=str,
                        metavar=('<path>'),
                        help="Deletes all files from marked in csv/xlsx files.")

    parser.add_argument("--formats", type=str, nargs="*",
                        metavar=('< arg... >'),
                        help="Enter file format as strings. Default is \'*\'. ")

    parser.add_argument("--addpaths", type=str, nargs="*",
                        metavar=('< path1, path2, .. >'),
                        help="Add additional path to include in search.")

    args = parser.parse_args()

    if args.compare:
        if args.formats:
            compare(args.compare[0], args.compare[1],
                    formats=args.formats)
        else:
            compare(args.compare[0], args.compare[1])

    if args.compareall:
        if args.formats:
            compareall(
                args.compareall[0], path2=args.compareall[1], formats=args.formats)
        else:
            compareall(args.compareall[0],
                       path2=args.compareall[1])

    elif args.comparealldirs:
        if args.addpaths:
            comparealldirs(
                args.comparealldirs[0], paths=args.addpaths)
        else:
            comparealldirs(args.comparealldirs[0])

    elif args.delete:
        delete_by_list(args.delete[0])

    else:
        print("USAGE: python3 filecleanup.py <password>")

    # os.system("pause")


if __name__ == "__main__":
    # calling the main function
    start_time = time.time()

    main()
    print("--- %s minutes ---" % ((time.time() - start_time)/60))
