# python -m pytest tests/

from pathlib import Path
import pytest
import pandas as pd
from src.filecleanup import delete_by_list, comparealldirs
from src.helper import paths_relation


def test_delete_from_compare(setup_files):
    # Test delete functionality based on compare result file.

    # Create the duplication information file
    data = {'original': [r"tests\originals\output-onlinejpgtools_grey.jpg"],
            'duplicate': [r"tests\duplicates\output-onlinejpgtools_grey.jpg"],
            'size': [15304],
            'to_be_deleted': ["y"]
            }

    pd.DataFrame(data).to_excel(r"tests\duplicates.xlsx")

    # Delete as per duplication information

    delete_by_list(r"tests\duplicates.xlsx")

    assert Path(
        r"tests\duplicates\output-onlinejpgtools_grey.jpg").exists() is False


def test_delete_from_compareall(setup_files):
    # Test delete functionality based on compare result file.

    # Create the duplication information file
    data = {

        'size': [15304],
        'duplicate_stem': ["output-onlinejpgtools_grey.jpg"],
        'count': [2],
        'path_0': [r"tests\duplicates\path0\output-onlinejpgtools_grey.jpg"],
        'to_be_deleted_0': [' '],
        'path_1': [r"tests\duplicates\path1\output-onlinejpgtools_grey.jpg"],
        'to_be_deleted_1': ['y']

    }

    pd.DataFrame(data).to_excel(r"tests\duplicates-all.xlsx")

    # Delete as per duplication information

    delete_by_list(r"tests\duplicates-all.xlsx")

    assert Path(
        r"tests\duplicates\path1\output-onlinejpgtools_grey.jpg").exists() is False

    assert Path(
        r"tests\duplicates\path0\output-onlinejpgtools_grey.jpg").exists() is True


def test_comparealldir(setup_dir):
    """Checks if func finds the right copies. Two identical directories
    are created.
    """
    comparealldirs("tests\duplicatesdirs", " ")

    if Path(r"duplicates-all.xlsx").exists() is True:
        duplicateinfo = pd.read_excel('duplicates-all.xlsx')
        print(duplicateinfo)
    else:
        raise Exception("The duplicate info file was not created.")

    assert duplicateinfo.shape == (2, 4)


def test_comparealldir_invalid_arg(setup_dir):
    """Checks if exception is rosen"""
    with pytest.raises(Exception):
        comparealldirs("tests\duplicatesdirs", "")


def test_path_relation():
    """Checks if two related paths as cli will raise exception. 
    """
    with pytest.raises(Exception):
        paths_relation("C:\\Users\\levinho\\Desktop\\Pam",
                       "C:\\Users\\levinho\\Desktop\\Pam\\Pamela")

    with pytest.raises(Exception):
        paths_relation("C:\\Users\\levinho\\Desktop\\Pam\\Pamela",
                       "C:\\Users\\levinho\\Desktop\\Pam")

    assert paths_relation("C:\\Users\\levinho\\Desktop\\Pam",
                          "C:\\Users\\levinho\\Desktop\\PamHartwig")

    assert paths_relation("C:\\Users\\levinho\\Desktop\\Pam",
                          "C:\\Users\\levinho\\Desktop\\PamHartwig")
