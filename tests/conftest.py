import pytest

import src.filecleanup
from pathlib import Path
import shutil


def _copy(self, target):
    assert self.is_file()
    shutil.copy(str(self), str(target))  # str() only there for Python < (3, 6)


Path.copy = _copy


@pytest.fixture(autouse=True)
def setup_files():
    Path(r"tests\duplicates").mkdir(exist_ok=True)
    Path(r"tests\duplicates\path0").mkdir(exist_ok=True)
    Path(r"tests\duplicates\path1").mkdir(exist_ok=True)

    original_files = list(Path(r"tests\originals").glob('*'))

    for file in original_files:
        Path(file).copy(r"tests\duplicates\path0")
        Path(file).copy(r"tests\duplicates\path1")

    yield

    existing_files = list(Path(r"tests\duplicates").glob('**/*.*'))
    for file in existing_files:
        Path(str(file)).unlink()


@pytest.fixture(autouse=True)
def setup_dir():
    Path(r"tests\duplicatesdirs").mkdir(exist_ok=True)
    Path(r"tests\duplicatesdirs\path0").mkdir(exist_ok=True)
    Path(r"tests\duplicatesdirs\path1").mkdir(exist_ok=True)

    original_files = list(Path(r"tests\originals").glob('*'))

    for file in original_files:
        Path(file).copy(r"tests\duplicatesdirs\path0")
        Path(file).copy(r"tests\duplicatesdirs\path1")

    yield

    existing_files = list(Path(r"tests\duplicatesdirs").glob('**/*.*'))
    for file in existing_files:
        Path(str(file)).unlink()
